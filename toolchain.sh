# GCC Toolchain that is primarily intended for static linking (dynamic is not really tested)
#
# To test locally e.g. run:
#     docker run -it -v D:\projects\allmine\toolchain:/mnt/d/ ubuntu bash
#     apt-get update && apt-get install -y build-essential wget file texinfo bison flex python
#     cd /root && /mnt/d/toolchain.sh
set -ex
SCRIPT_DIR=$(dirname $(readlink -f $0))

# target of the compiler toolchain
TARGET=x86_64-linux-musl
# target of the final compiler
STAGE2_TARGET=${STAGE2_TARGET:-"$TARGET"}
GCC_VERSION=9.1.0
BINUTILS_VERSION=2.32
MUSL_VERSION=1.1.22
LINUX_VERSION=4.14.115

BINUTILS_FLAGS="--disable-nls --disable-multilib --disable-werror --disable-gdb"
GCC_FLAGS="--disable-werror --disable-libgomp --disable-nls --enable-languages=c,c++ --disable-multilib --disable-shared"
MUSL_FLAGS_STAGE1="--enable-optimize=size"
# MUSL_FLAGS_STAGE2="--enable-optimize=size --enable-debug"

STAGE1_PREFIX=$(pwd)/stage1-toolchain
STAGE2_PREFIX=$(pwd)/stage2-toolchain
LIBGCC_A=$STAGE1_PREFIX/lib/gcc/$TARGET/$GCC_VERSION/libgcc.a
BINUTILS_DIR=binutils-$BINUTILS_VERSION

MAKE_CMD=${MAKE_CMD:-"make -j4"}
MAKE_CMD_STDOUT="/dev/stdout"
if [ -n "$MAKE_SILENT" ]; then
    MAKE_CMD_STDOUT="/dev/null"
fi

if [ ! -d gcc-$GCC_VERSION ]; then
    echo "Downloading GCC $GCC_VERSION"
    wget ftp://ftp.fu-berlin.de/unix/languages/gcc/releases/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.xz -N -O gcc-$GCC_VERSION.tar.xz
    tar xf gcc-$GCC_VERSION.tar.xz

    pushd gcc-$GCC_VERSION
    # FIXME/PORTME/TODO patch -p3 < $SCRIPT_DIR/musl-libsanitizer-gcc-8.2.0.patch
    contrib/download_prerequisites
    popd
fi

if [ ! -d $STAGE1_PREFIX/include/linux ]; then
    echo "Downloading Linux Kernel $LINUX_VERSION & Installing Headers"
    wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$LINUX_VERSION.tar.xz -N -O linux-$LINUX_VERSION.tar.xz
    tar xf linux-$LINUX_VERSION.tar.xz
    pushd linux-$LINUX_VERSION
    make INSTALL_HDR_PATH=$STAGE1_PREFIX headers_install
    popd
fi

if [ ! -d $BINUTILS_DIR ]; then
    echo "Downloading Binutils $BINUTILS_VERSION"
    wget https://ftp-stud.hs-esslingen.de/Mirrors/ftp.gnu.org/binutils/binutils-$BINUTILS_VERSION.tar.xz -O binutils-$BINUTILS_VERSION.tar.xz
    # wget https://github.com/bminor/binutils-gdb/archive/$BINUTILS_VERSION.tar.gz -N -O binutils-$BINUTILS_VERSION.tar.gz
    tar xf binutils-$BINUTILS_VERSION.tar.xz
fi

if [ ! -f $STAGE1_PREFIX/bin/$TARGET-ar ]; then
    echo "Building Binutils $BINUTILS_VERSION"
    mkdir -p stage1/binutils
    pushd stage1/binutils
    CFLAGS="-march=native" ../../$BINUTILS_DIR/configure --prefix=$STAGE1_PREFIX --target=$TARGET $BINUTILS_FLAGS
    $MAKE_CMD > $MAKE_CMD_STDOUT
    make install
    popd
fi

XGCC_DIR=$(pwd)/stage1/gcc/gcc/
XGCC=$XGCC_DIR/xgcc
if [ ! -f $XGCC ]; then
    echo "Building GCC $GCC_VERSION (stage1 - xgcc)"
    mkdir -p stage1/gcc
    pushd stage1/gcc
    # Disable libmpx, for the 1st stage we do not (always) have a libc
    CFLAGS="-march=native" PATH="$PATH:/tmp/root/stage1-toolchain/bin/" ../../gcc-$GCC_VERSION/configure --prefix=$STAGE1_PREFIX --target=$TARGET $GCC_FLAGS \
        --with-sysroot=$STAGE1_PREFIX \
        --with-native-system-header-dir=/include --disable-libmpx --disable-libsanitizer
    $MAKE_CMD all-gcc > $MAKE_CMD_STDOUT
    popd
fi

if [ ! -f $STAGE1_PREFIX/include/stdio.h ]; then
    echo "Setting up MUSL - Downloading & Copying headers for stage1 build"
    wget https://www.musl-libc.org/releases/musl-$MUSL_VERSION.tar.gz -N -O musl-$MUSL_VERSION.tar.gz
    # wget https://git.musl-libc.org/cgit/musl/snapshot/musl-$MUSL_VERSION.tar.gz
    tar xzf musl-$MUSL_VERSION.tar.gz
    mkdir -p stage1/musl
    pushd stage1/musl
    CC="$XGCC -B $XGCC_DIR" LIBCC="$LIBGCC_A" \
        ../../musl-$MUSL_VERSION/configure --disable-shared --prefix=$STAGE1_PREFIX --target=$TARGET $MUSL_FLAGS_STAGE1
    make install-headers
    popd
fi

if [ ! -f $LIBGCC_A ]; then
    echo "Building GCC $GCC_VERSION (stage1 - libgcc.a)"
    pushd stage1/gcc
    $MAKE_CMD all-target-libgcc > $MAKE_CMD_STDOUT
    make install-target-libgcc
    popd
fi

if [ ! -f $STAGE1_PREFIX/lib/libc.a ]; then
    echo "Building MUSL $MUSL_VERSION"
    pushd stage1/musl
    PATH=$PATH:$STAGE1_PREFIX/bin \
        $MAKE_CMD > $MAKE_CMD_STDOUT
    make install
    popd
fi

if [ ! -f $STAGE1_PREFIX/bin/$TARGET-gcc ]; then
    echo "Building GCC $GCC_VERSION (stage 1)"
    pushd stage1/gcc
    $MAKE_CMD > $MAKE_CMD_STDOUT
    make install
    popd
fi

if [ ! -d $STAGE2_PREFIX/include/linux ]; then
    echo "Installing Linux Kernel $LINUX_VERSION Headers (to stage2)"
    pushd linux-$LINUX_VERSION
    make INSTALL_HDR_PATH=$STAGE2_PREFIX headers_install
    popd
fi

# Stage 1 is done, we have a working cross-compiler
# now build a staticly linked toolchain
PATH="$PATH:$STAGE1_PREFIX/bin/"
if [ "$TARGET" != "$STAGE2_TARGET" ]; then
    AR=$STAGE2_TARGET-ar
else
    AR="ar"
fi
if [ ! -f $STAGE2_PREFIX/bin/$AR ]; then
    echo "Building Binutils $BINUTILS_VERSION (stage2, static)"
    mkdir -p stage2/binutils
    pushd stage2/binutils
    CFLAGS="-static" LDFLAGS="--static" ../../$BINUTILS_DIR/configure --prefix=$STAGE2_PREFIX --host=$TARGET --target=$STAGE2_TARGET $BINUTILS_FLAGS
    $MAKE_CMD > $MAKE_CMD_STDOUT
    make install
    popd
fi

# If the target is different, we use the stage1 compiler 
# (which we built so we can build a static compiler for the build architecture we intend to use)
# to bootstrap another architecture, which is more effort.
# ...
if [ "$TARGET" != "$STAGE2_TARGET" ]; then
    XGCC_DIR_STAGE2=$(pwd)/stage2/gcc/gcc/
    XGCC_STAGE2=$XGCC_DIR_STAGE2/xgcc
    LIBGCC_A_STAGE2=$STAGE2_PREFIX/lib/gcc/$STAGE2_TARGET/$GCC_VERSION/libgcc.a
    PATH="$STAGE2_PREFIX/bin:$STAGE2_PREFIX/$STAGE2_TARGET/bin:$PATH"

    if [ ! -f $XGCC_STAGE2 ]; then
        echo "Building GCC $GCC_VERSION (stage2 - xgcc, STAGE2_TARGET bootstrap)"
        mkdir -p stage2/gcc
        pushd stage2/gcc
        CFLAGS="-static" LDFLAGS="--static" ../../gcc-$GCC_VERSION/configure --prefix=$STAGE2_PREFIX \
            --build=$TARGET \
            --host=$TARGET \
            --target=$STAGE2_TARGET $GCC_FLAGS \
            --with-native-system-header-dir=/include --disable-libsanitizer
        $MAKE_CMD all-gcc > $MAKE_CMD_STDOUT
        popd
    fi

    if [ ! -f $STAGE2_PREFIX/include/stdio.h ]; then
        echo "Setting up MUSL - Copying headers for stage2 build"
        mkdir -p stage2/musl
        pushd stage2/musl
        CC="$XGCC_STAGE2 -B $XGCC_DIR_STAGE2" LIBCC="$LIBGCC_A_STAGE2" \
            ../../musl-$MUSL_VERSION/configure --disable-shared --prefix=$STAGE2_PREFIX/$STAGE2_TARGET/ --build=$TARGET --host=$TARGET --target=$STAGE2_TARGET $MUSL_FLAGS_STAGE2
        make install-headers
        popd
    fi

    if [ ! -f $LIBGCC_A_STAGE2 ]; then
        echo "Building GCC $GCC_VERSION (stage2 - libgcc.a)"
        pushd stage2/gcc
        $MAKE_CMD all-target-libgcc > $MAKE_CMD_STDOUT
        make install-target-libgcc
        popd
    fi

    if [ ! -f $STAGE2_PREFIX/lib/libc.a ]; then
        echo "Building MUSL $MUSL_VERSION (stage2)"
        pushd stage2/musl
        $MAKE_CMD > $MAKE_CMD_STDOUT
        make install
        popd
    fi

    if [ ! -f $STAGE2_PREFIX/bin/$STAGE2_TARGET-gcc ]; then
        echo "Building GCC $GCC_VERSION (stage2, stripped)"
        pushd stage2/gcc
        $MAKE_CMD > $MAKE_CMD_STDOUT
        make install-strip
        popd
    fi

# If the target is the same, we can partially reuse artifacts
# and only have to put the full compiler together
else
    if [ ! -f $STAGE2_PREFIX/lib/libc.a ]; then
        echo "Building MUSL $MUSL_VERSION (stage2)"
        mkdir -p stage2/musl
        pushd stage2/musl
        ../../musl-$MUSL_VERSION/configure --prefix=$STAGE2_PREFIX --host=$TARGET --target=$STAGE2_TARGET $MUSL_FLAGS_STAGE2
        $MAKE_CMD > $MAKE_CMD_STDOUT
        make install
        popd
    fi

    if [ ! -f $STAGE2_PREFIX/bin/gcc ]; then
        echo "Building GCC $GCC_VERSION (stage2 - musl, static, stripped)"
        mkdir -p stage2/gcc
        pushd stage2/gcc
        CFLAGS="-static" LDFLAGS="--static" ../../gcc-$GCC_VERSION/configure --prefix=$STAGE2_PREFIX --host=$TARGET --target=$STAGE2_TARGET $GCC_FLAGS \
            --with-native-system-header-dir=/include --disable-libsanitizer
        $MAKE_CMD > $MAKE_CMD_STDOUT
        make install-strip
        popd
    fi
fi

FILENAME="$STAGE2_TARGET-$MUSL_VERSION-gcc-$GCC_VERSION-static$FILENAME_SUFFIX"
echo "Build archive $FILENAME"
tar --hard-dereference --transform="flags=r;s|stage2-toolchain|$FILENAME|" -cf - stage2-toolchain/* | xz -0 -z - > $FILENAME.tar.xz
