set -e
rm -rf stage2*
export MUSL_FLAGS_STAGE2="--enable-optimize=size"
export FILENAME_SUFFIX="-$CI_JOB_ID"
bash ./toolchain.sh

#rm -rf stage2-toolchain stage2/gcc stage2/musl
#MUSL_FLAGS_STAGE2="--enable-optimize=size --enable-debug"
#FILENAME_SUFFIX="-optdebug-$CI_JOB_ID"
#bash ./toolchain.sh

rm -rf stage2-toolchain stage2/gcc stage2/musl
export MUSL_FLAGS_STAGE2="--enable-debug"
export FILENAME_SUFFIX="-debug-$CI_JOB_ID"
bash ./toolchain.sh
