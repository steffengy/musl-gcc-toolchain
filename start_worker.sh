# Steps to run a gitlab CI worker locally
# Configure Runner
docker run --rm -t -i -v gitlab-runner-cfg:/etc/gitlab-runner --name gitlab-runner gitlab/gitlab-runner register

# Start Runner
docker run -d --name gitlab-runner --restart always \
   -v gitlab-runner-cfg:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   gitlab/gitlab-runner:latest
